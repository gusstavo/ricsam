using System;
using System.Collections.Generic;

namespace Ricsman.Entities
{
    public partial class SubProyecto
    {
        public SubProyecto()
        {
            this.Facturas = new List<Factura>();
            this.SubProyecto1 = new List<SubProyecto>();
        }

        public int IdSubProyecto { get; set; }
        public string NomSubProy { get; set; }
        public Nullable<int> IdProyecto { get; set; }
        public Nullable<int> IdProyPadre { get; set; }
        public virtual ICollection<Factura> Facturas { get; set; }
        public virtual Proyecto Proyecto { get; set; }
        public virtual ICollection<SubProyecto> SubProyecto1 { get; set; }
        public virtual SubProyecto SubProyecto2 { get; set; }
    }
}
