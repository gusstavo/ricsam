using System;
using System.Collections.Generic;

namespace Ricsman.Entities
{
    public partial class Producto
    {
        public Producto()
        {
            this.Facturas = new List<Factura>();
        }

        public int IdProducto { get; set; }
        public string CodigoProducto { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string DescripcionProducto { get; set; }
        public decimal UnidadEmpaque { get; set; }
        public decimal UnidadMedida { get; set; }
        public decimal IGV { get; set; }
        public virtual Proveedor Proveedor { get; set; }
        public Nullable<int> IcCategoria { get; set; }
        public virtual Categoria Categoria { get; set; }
        //datos de venta
        public Nullable<decimal> CostoInventario { get; set; }
        public Nullable<decimal> CostoPomedio { get; set; }
        public Nullable<decimal> CostoActual{ get; set; }
        public Nullable<decimal> CostoReal { get; set; }
        //restriciones
        public virtual ICollection<Factura> Facturas { get; set; }
        
    }
}
