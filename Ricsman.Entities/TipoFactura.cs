using System;
using System.Collections.Generic;

namespace Ricsman.Entities
{
    public partial class TipoFactura
    {
        public TipoFactura()
        {
            this.Facturas = new List<Factura>();
        }

        public int IdTipFact { get; set; }
        public string Nombre { get; set; }
        public Nullable<decimal> ValorTipFac { get; set; }
        public virtual ICollection<Factura> Facturas { get; set; }
    }
}
