using System;
using System.Collections.Generic;

namespace Ricsman.Entities
{
    public partial class Categoria
    {
        public Categoria()
        {
            this.Productoes = new List<Producto>();
        }

        public int IdCategoria { get; set; }
        public string NomCategoria { get; set; }
        public virtual ICollection<Producto> Productoes { get; set; }
    }
}
