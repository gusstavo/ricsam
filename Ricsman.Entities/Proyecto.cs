using System;
using System.Collections.Generic;

namespace Ricsman.Entities
{
    public partial class Proyecto
    {
        public Proyecto()
        {
            this.SubProyectoes = new List<SubProyecto>();
        }

        public int IdProyecto { get; set; }
        public string NomProyecto { get; set; }
        public virtual ICollection<SubProyecto> SubProyectoes { get; set; }
    }
}
