using System;
using System.Collections.Generic;

namespace Ricsman.Entities
{
    public partial class Proveedor
    {
        public Proveedor()
        {
            this.Facturas = new List<Factura>();
        }

        public int IdProveedo { get; set; }
        public string NomProveedor { get; set; }
        public string RucProveedor { get; set; }
        public string TefProveedor { get; set; }
        public string DirProveedor { get; set; }
        public virtual ICollection<Factura> Facturas { get; set; }
    }
}
