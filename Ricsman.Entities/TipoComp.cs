using System;
using System.Collections.Generic;

namespace Ricsman.Entities
{
    public partial class TipoComp
    {
        public TipoComp()
        {
            this.Facturas = new List<Factura>();
        }

        public int IdTipoComp { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<Factura> Facturas { get; set; }
    }
}
