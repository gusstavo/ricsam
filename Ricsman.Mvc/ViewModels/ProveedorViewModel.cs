﻿using System.Collections.Generic;

namespace Ricsman.Mvc.ViewModels
{
    public class ProveedorViewModel
    {
        public int IdProveedo { get; set; }
        public string NomProveedor { get; set; }
        public string RucProveedor { get; set; }
        public string TefProveedor { get; set; }
        public string DirProveedor { get; set; }
        public virtual ICollection<FacturaViewModel> Facturas { get; set; }
    }
}