﻿using System.Collections.Generic;

namespace Ricsman.Mvc.ViewModels
{
    public class TipoCompViewModel
    {
        public int IdTipoComp { get; set; }
        public string Nombre { get; set; }
        public virtual ICollection<FacturaViewModel> Facturas { get; set; }
    }
}