﻿using System;
using System.Collections.Generic;

namespace Ricsman.Mvc.ViewModels
{
    public class TipoFacturaViewModel
    {
        public int IdTipFact { get; set; }
        public string Nombre { get; set; }
        public Nullable<decimal> ValorTipFac { get; set; }
        public virtual ICollection<FacturaViewModel> Facturas { get; set; }
    }
}