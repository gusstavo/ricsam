﻿using System;
using System.Collections.Generic;


namespace Ricsman.Mvc.ViewModels
{
    public class FacturaViewModel
    {
        public int IdFactura { get; set; }
        public string SerieFactura { get; set; }
        public string NumFactura { get; set; }
        public Nullable<decimal> Subtotal { get; set; }
        public Nullable<decimal> Igv { get; set; }
        public Nullable<decimal> Total { get; set; }
        public Nullable<int> IdProveedor { get; set; }
        public Nullable<int> IdSubProy { get; set; }
        public Nullable<int> IdTipoComp { get; set; }
        public Nullable<decimal> Moneda { get; set; }
        public Nullable<System.DateTime> FechaFac { get; set; }
        public Nullable<int> idTipoFact { get; set; }
        public Nullable<decimal> CalculoDetrac { get; set; }
        public string RegimenTrib { get; set; }
        public Nullable<bool> TipPago { get; set; }
        public Nullable<System.DateTime> FecVencimiento { get; set; }
        public string Estado { get; set; }
        public string MedCanc { get; set; }
        public string NumCheq { get; set; }
        public virtual ProveedorViewModel Proveedor { get; set; }
        public virtual SubProyectoViewModel SubProyecto { get; set; }
        public virtual TipoCompViewModel TipoComp { get; set; }
        public virtual TipoFacturaViewModel TipoFactura { get; set; }
        public virtual ICollection<ProductoViewModel> Productoes { get; set; }
    }
}