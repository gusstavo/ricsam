﻿using System;
using System.Collections.Generic;

namespace Ricsman.Mvc.ViewModels
{
    public class SubProyectoViewModel
    {
        public int IdSubProyecto { get; set; }
        public string NomSubProy { get; set; }
        public Nullable<int> IdProyecto { get; set; }
        public Nullable<int> IdProyPadre { get; set; }
        public virtual ICollection<FacturaViewModel> Facturas { get; set; }
        public virtual ProductoViewModel Proyecto { get; set; }
        public virtual ICollection<SubProyectoViewModel> SubProyecto1 { get; set; }
        public virtual SubProyectoViewModel SubProyecto2 { get; set; }
    }
}