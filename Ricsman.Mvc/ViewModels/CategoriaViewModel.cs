﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ricsman.Mvc.ViewModels
{
    public class CategoriaViewModel
    {
        public int IdCategoria { get; set; }
        public string NomCategoria { get; set; }
    }
}