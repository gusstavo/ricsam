﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ricsman.Mvc.ViewModels
{
    public class ProductoViewModel
    {
        public int IdProducto { get; set; }
        public string CodigoProducto { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string DescripcionProducto { get; set; }
        public decimal UnidadEmpaque { get; set; }
        public decimal UnidadMedida { get; set; }
        public decimal IGV { get; set; }
        public virtual ProveedorViewModel Proveedor { get; set; }
        public Nullable<int> IdCategoria { get; set; }
        public virtual CategoriaViewModel Categoria { get; set; }
        //datos de venta
        public Nullable<decimal> CostoInventario { get; set; }
        public Nullable<decimal> CostoPomedio { get; set; }
        public Nullable<decimal> CostoActual { get; set; }
        public Nullable<decimal> CostoReal { get; set; }
        //restriciones
        public virtual ICollection<FacturaViewModel> Facturas { get; set; }
       
    }
}