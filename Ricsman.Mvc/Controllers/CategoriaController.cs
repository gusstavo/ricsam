﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ricsman.Entities;

namespace Ricsman.Mvc.Controllers
{
    public class CategoriaController : Controller
    {
        // GET: Categoria
        public ActionResult Index()
        {
            var categoria = new List<Categoria>()
            {
                new Categoria()
                {
                    IdCategoria = 1,
                    NomCategoria = "Mi Categoria"
                },
                new Categoria()
                {
                    IdCategoria = 2,
                    NomCategoria = "Mi Categoria 2"
                }
            }.ToList();
            return View(categoria);
        }

        public ActionResult Create()
        {
            return View();
        }
    }
}