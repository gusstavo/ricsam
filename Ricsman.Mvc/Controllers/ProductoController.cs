﻿using Ricsman.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ricsman.Mvc.Controllers
{
    public class ProductoController : Controller
    {
        // GET: Producto
        public ActionResult Index()
        {
            var producto = new List<Producto>()
            {
                new Producto()
                {
                    IdProducto = 1,
                    CodigoProducto = "Cod-001",
                    FechaCreacion = DateTime.Now,
                    DescripcionProducto = "PRODUCTO 1",
                    UnidadEmpaque = 1,
                    UnidadMedida = 2,
                    IGV = 18,
                    Proveedor = new Proveedor()
                    {
                        IdProveedo = 1,
                        NomProveedor ="Proveedor 001",
                        DirProveedor ="Jr. Proceres #5454"
                    },
                    CostoInventario = 2
                    
                },
                new Producto()
                {
                    IdProducto = 2,
                    CodigoProducto = "Cod-002",
                    FechaCreacion = DateTime.Now,
                    DescripcionProducto = "PRODUCTO 2",
                    UnidadEmpaque = 1,
                    UnidadMedida = 2,
                    IGV = 18,
                    Proveedor = new Proveedor()
                    {
                        IdProveedo = 2,
                        NomProveedor ="Proveedor 002",
                        DirProveedor ="Jr. Proceres #5454"
                    },
                    CostoInventario = 2

                }
            }.ToList();
            return View(producto);
        }
        public ActionResult Create()
        {
            return View();
        }
    }
}