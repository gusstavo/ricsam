﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ricsman.Entities;

namespace Ricsman.Mvc.Controllers
{
    public class FacturaController : Controller
    {
        // GET: Factura
        public ActionResult Index()
        {
            var factura = new List<Factura>()
            {
                new Factura()
                {
                    IdFactura = 1,
                    SerieFactura ="cod-001",
                    NumFactura = "c-2343",
                    Subtotal = 123,
                    Igv = 123,
                    FechaFac = DateTime.Now,
                    FecVencimiento = DateTime.Now,
                    RegimenTrib ="001"
                },
                new Factura()
                {
                    IdFactura = 2,
                    SerieFactura ="cod-001",
                    NumFactura = "c-2343",
                    Subtotal = 123,
                    Igv = 123,
                    FechaFac = DateTime.Now,
                    FecVencimiento = DateTime.Now,
                    RegimenTrib ="001"
                }
            }.ToList();

            return View(factura);
        }

        public ActionResult Create()
        {
            return View();
        }
    }
}