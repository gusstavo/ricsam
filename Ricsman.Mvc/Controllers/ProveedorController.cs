﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ricsman.Entities;

namespace Ricsman.Mvc.Controllers
{
    public class ProveedorController : Controller
    {
        // GET: Proveedor
        public ActionResult Index()
        {
            var proveedor = new List<Proveedor>()
            {
                new Proveedor()
                {
                    IdProveedo = 1,
                    NomProveedor = "Mi Proveedor"
                },
                new Proveedor()
                {
                    IdProveedo = 2,
                    NomProveedor = "Mi Proveedor 2"
                }
            }.ToList();
            return View(proveedor);
        }

       public ActionResult Create()
        {
            return View();
        }
    }
}