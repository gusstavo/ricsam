﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ricsman.Mvc.Startup))]
namespace Ricsman.Mvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
