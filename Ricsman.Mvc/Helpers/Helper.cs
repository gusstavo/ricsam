﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Ricsman.Mvc.Helpers
{
    public static class Helper
    {
        public static MvcHtmlString IfDatetimeFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" + LabelFor(html, expression, required) +
                         string.Format("<div class=\"col-lg-{0} \">", cols) +
                         "<div class=\"input-group\">" +
                         html.TextBoxFor(expression, "{0:dd/MM/yyyy HH:mm}",
                             new
                             {
                                 @class = "form-control dateIf",
                                 autocomplete = "off",
                                 data_format = "DD/MM/YYYY HH::mm"
                             }) +
                         "<span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>" +
                         "</div>" +
                         "</div>" +

                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }
        public static MvcHtmlString IfDatetimeCustomerNameFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, string name, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" + LabelCustomerFor(html, name, required) +
                         string.Format("<div class=\"col-lg-{0} \">", cols) +
                         "<div class=\"input-group\">" +
                         html.TextBoxFor(expression, "{0:dd/MM/yyyy HH:mm}",
                             new
                             {
                                 @class = "form-control dateIf",
                                 autocomplete = "off",
                                 data_format = "DD/MM/YYYY HH::mm"
                             }) +
                         "<span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>" +
                         "</div>" +
                         "</div>" +

                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }
        public static MvcHtmlString IfTextBoxFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" + LabelFor(html, expression, required) +
                         string.Format("<div class=\"col-lg-{0}\">", cols) +
                         html.TextBoxFor(expression, new { @class = "form-control", autocomplete = "off" }) +
                         "</div>" +
                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }


       
        public static MvcHtmlString LabelFor<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, bool required)
        {
            return html.LabelFor(expression, new { @class = "control-label col-md-3 " + (required ? "required" : "") });
        }
        private static bool IsRequired<T, V>(this Expression<Func<T, V>> expression)
        {
            var memberExpression = expression.Body as MemberExpression;
            if (memberExpression == null)
                throw new InvalidOperationException("Expression must be a member expression");

            return memberExpression.Member.GetAttribute<RequiredAttribute>() != null;
        }
        private static T GetAttribute<T>(this ICustomAttributeProvider provider)
            where T : Attribute
        {
            var attributes = provider.GetCustomAttributes(typeof(T), true);
            return attributes.Length > 0 ? attributes[0] as T : null;
        }

        public static MvcHtmlString IfDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            string display = "<div class='form-group'>" +
                                 "<div class='col-sm-3'>" +
                                     "<strong>" + html.DisplayNameFor(expression) + "</strong>" +
                                 "</div>" +

                                 "<div class='col-sm-7'>" +
                                     html.DisplayFor(expression) +
                                 "</div>" +
                             "</div>";

            return new MvcHtmlString(display);
        }

        public static MvcHtmlString IfTextBoxCustomerNameFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, string name, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" + LabelCustomerFor(html, name, required) +
                         string.Format("<div class=\"col-lg-{0}\">", cols) +
                         html.TextBoxFor(expression, new { @class = "form-control", autocomplete = "off" }) +
                         "</div>" +
                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }
        public static MvcHtmlString LabelCustomerFor<TModel>(HtmlHelper<TModel> html, string name, bool required)
        {
            return html.Label(name, new { @class = "control-label col-md-3 " + (required ? "required" : "") });
        }
        public static MvcHtmlString IfOnlyTextBoxPlaceholderFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, string placeholders, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" +
                         string.Format("<div class=\"col-lg-{0}\">", cols) +
                         html.TextBoxFor(expression, new { @class = "form-control", placeholder = placeholders, autocomplete = "off" }) +
                         "</div>" +
                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }
        public static MvcHtmlString IfTextBoxNumberCusNameFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, string name, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" + LabelCustomerFor(html, name, required) +
                         string.Format("<div class=\"col-lg-{0}\">", cols) +
                         html.TextBoxFor(expression, new { @class = "form-control decimalIf", autocomplete = "off" }) +
                         "</div>" +
                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }

        public static MvcHtmlString IfDatetimeMayorNowFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" + LabelFor(html, expression, required) +
                         string.Format("<div class=\"col-lg-{0} \">", cols) +
                         "<div class=\"input-group\">" +
                         html.TextBoxFor(expression, "{0:dd/MM/yyyy HH:mm}",
                             new
                             {
                                 @class = "form-control dateIfValid",
                                 autocomplete = "off",
                                 data_format = "DD/MM/YYYY HH::mm"
                             }) +
                         "<span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>" +
                         "</div>" +
                         "</div>" +

                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }
        public static MvcHtmlString IfDatetimeCustomerNameMayorNowFor<TModel, TValue>(this HtmlHelper<TModel> html,
            Expression<Func<TModel, TValue>> expression, string name, bool required, int cols = 12)
        {
            var editor = "<div class=\"form-group\">" + LabelCustomerFor(html, name, required) +
                         string.Format("<div class=\"col-lg-{0} \">", cols) +
                         "<div class=\"input-group\">" +
                         html.TextBoxFor(expression, "{0:dd/MM/yyyy HH:mm}",
                             new
                             {
                                 @class = "form-control dateIfValid",
                                 autocomplete = "off",
                                 data_format = "DD/MM/YYYY HH::mm"
                             }) +
                         "<span class=\"input-group-addon\"><i class=\"fa fa-calendar\"></i></span>" +
                         "</div>" +
                         "</div>" +

                            html.ValidationMessageFor(expression, String.Empty, new { @class = "help-inline" }) +
                        "</div>";
            return new MvcHtmlString(editor);
        }

        public static IDisposable IfBeginPanelGeneric(this HtmlHelper html, int size, string title = null)
        {
            return CoreHelpers.IfGBeginPanel(html, size, title);
        }
    }
}