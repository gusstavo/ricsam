﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ricsman.Mvc.Helpers
{
    public static class CoreHelpers
    {
        public static IDisposable IfBeginPanel(this HtmlHelper html, int size = 12, string title = null)
        {
            var htmlTitle = title != null ? String.Format("<h3 class='box - title'>{0}</h3>", title) : "";

            var heder = String.Format("<div class='col-md-{0}'>" +
                                          "<div class='box box-primary'>" +
                                            "<div class='box-header with-border'>" +
                                                "<div class='panel-tools'>" +
                                                    "<a class='showhide'><i class='fa fa-chevron-up'></i></a>" +
                                                    "<a class='closebox'><i class='fa fa-times'></i></a>" +
                                                "</div>" +
                                                "<br />{1}" +
                                             "</div>", size, htmlTitle +
                                             "</div>");

            html.ViewContext.Writer.Write(String.Format("{0} <br/><div class='panel-body'>", heder));


            return new PanelView(html);
        }
        public static IDisposable IfGBeginPanel(this HtmlHelper html, int size = 12, string title = null, object action = null)
        {
            dynamic d = action;
            var htmlAction = action != null ? string.Format("<a class='btn btn-primary pull-right' href='{0}'>{1}</a>", 
                action.GetType().GetProperty("href").GetValue(action, null), 
                action.GetType().GetProperty("text").GetValue(action, null)) : "";

            var htmlTitle = title != null ? string.Format("<div class='box-header with-border'><h3 class='box-title'>{0}</h3>{1}<div class='clearfix'></div></div>", title, htmlAction) : "";
            html.ViewContext.Writer.Write(string.Format("<div class='col-md-{0}'><div class='box box-primary'>{1}<div class='box-body'>", size ,htmlTitle));

            return new PanelView(html);
        }

        class PanelView : IDisposable
        {
            private HtmlHelper helper;

            public PanelView(HtmlHelper helper)
            {
                this.helper = helper;
            }

            public void Dispose()
            {
                this.helper.ViewContext.Writer.Write("</div></div></div>");
            }

        }

        public static string ValidationMessageFor(string name)
        {
            return "<span class='field-validation-valid' </span>";
        }
    }
}