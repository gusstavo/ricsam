using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Ricsman.Entities;
using Ricsman.Repository.Mapping;

namespace Ricsman.Repository
{
    public partial class BdRicsamContext : DbContext
    {
        static BdRicsamContext()
        {
            Database.SetInitializer<BdRicsamContext>(null);
        }

        public BdRicsamContext()
            : base("Name=BdRicsamContext")
        {
        }

        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Factura> Facturas { get; set; }
        public DbSet<Producto> Productoes { get; set; }
        public DbSet<Proveedor> Proveedors { get; set; }
        public DbSet<Proyecto> Proyectoes { get; set; }
        public DbSet<SubProyecto> SubProyectoes { get; set; }
        public DbSet<TipoComp> TipoComps { get; set; }
        public DbSet<TipoFactura> TipoFacturas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CategoriaMap());
            modelBuilder.Configurations.Add(new FacturaMap());
            modelBuilder.Configurations.Add(new ProductoMap());
            modelBuilder.Configurations.Add(new ProveedorMap());
            modelBuilder.Configurations.Add(new ProyectoMap());
            modelBuilder.Configurations.Add(new SubProyectoMap());
            modelBuilder.Configurations.Add(new TipoCompMap());
            modelBuilder.Configurations.Add(new TipoFacturaMap());
        }
    }
}
