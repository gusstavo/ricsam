using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class TipoFacturaMap : EntityTypeConfiguration<TipoFactura>
    {
        public TipoFacturaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipFact);

            // Properties
            this.Property(t => t.Nombre)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TipoFactura");
            this.Property(t => t.IdTipFact).HasColumnName("IdTipFact");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
            this.Property(t => t.ValorTipFac).HasColumnName("ValorTipFac");
        }
    }
}
