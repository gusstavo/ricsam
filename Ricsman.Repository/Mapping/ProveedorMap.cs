using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class ProveedorMap : EntityTypeConfiguration<Proveedor>
    {
        public ProveedorMap()
        {
            // Primary Key
            this.HasKey(t => t.IdProveedo);

            // Properties
            this.Property(t => t.RucProveedor)
                .IsFixedLength()
                .HasMaxLength(11);

            this.Property(t => t.TefProveedor)
                .HasMaxLength(9);

            // Table & Column Mappings
            this.ToTable("Proveedor");
            this.Property(t => t.IdProveedo).HasColumnName("IdProveedo");
            this.Property(t => t.NomProveedor).HasColumnName("NomProveedor");
            this.Property(t => t.RucProveedor).HasColumnName("RucProveedor");
            this.Property(t => t.TefProveedor).HasColumnName("TefProveedor");
            this.Property(t => t.DirProveedor).HasColumnName("DirProveedor");
        }
    }
}
