using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class SubProyectoMap : EntityTypeConfiguration<SubProyecto>
    {
        public SubProyectoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdSubProyecto);

            // Properties
            // Table & Column Mappings
            this.ToTable("SubProyecto");
            this.Property(t => t.IdSubProyecto).HasColumnName("IdSubProyecto");
            this.Property(t => t.NomSubProy).HasColumnName("NomSubProy");
            this.Property(t => t.IdProyecto).HasColumnName("IdProyecto");
            this.Property(t => t.IdProyPadre).HasColumnName("IdProyPadre");

            // Relationships
            this.HasOptional(t => t.Proyecto)
                .WithMany(t => t.SubProyectoes)
                .HasForeignKey(d => d.IdProyecto);
            this.HasOptional(t => t.SubProyecto2)
                .WithMany(t => t.SubProyecto1)
                .HasForeignKey(d => d.IdProyPadre);

        }
    }
}
