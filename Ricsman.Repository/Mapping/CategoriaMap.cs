using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class CategoriaMap : EntityTypeConfiguration<Categoria>
    {
        public CategoriaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdCategoria);

            // Properties
            // Table & Column Mappings
            this.ToTable("Categoria");
            this.Property(t => t.IdCategoria).HasColumnName("IdCategoria");
            this.Property(t => t.NomCategoria).HasColumnName("NomCategoria");
        }
    }
}
