using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class TipoCompMap : EntityTypeConfiguration<TipoComp>
    {
        public TipoCompMap()
        {
            // Primary Key
            this.HasKey(t => t.IdTipoComp);

            // Properties
            // Table & Column Mappings
            this.ToTable("TipoComp");
            this.Property(t => t.IdTipoComp).HasColumnName("IdTipoComp");
            this.Property(t => t.Nombre).HasColumnName("Nombre");
        }
    }
}
