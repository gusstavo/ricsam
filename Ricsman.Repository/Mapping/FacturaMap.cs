using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class FacturaMap : EntityTypeConfiguration<Factura>
    {
        public FacturaMap()
        {
            // Primary Key
            this.HasKey(t => t.IdFactura);

            // Properties
            this.Property(t => t.SerieFactura)
                .HasMaxLength(3);

            this.Property(t => t.NumFactura)
                .HasMaxLength(6);

            this.Property(t => t.RegimenTrib)
                .HasMaxLength(50);

            this.Property(t => t.Estado)
                .HasMaxLength(50);

            this.Property(t => t.MedCanc)
                .HasMaxLength(50);

            this.Property(t => t.NumCheq)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Factura");
            this.Property(t => t.IdFactura).HasColumnName("IdFactura");
            this.Property(t => t.SerieFactura).HasColumnName("SerieFactura");
            this.Property(t => t.NumFactura).HasColumnName("NumFactura");
            this.Property(t => t.Subtotal).HasColumnName("Subtotal");
            this.Property(t => t.Igv).HasColumnName("Igv");
            this.Property(t => t.Total).HasColumnName("Total");
            this.Property(t => t.IdProveedor).HasColumnName("IdProveedor");
            this.Property(t => t.IdSubProy).HasColumnName("IdSubProy");
            this.Property(t => t.IdTipoComp).HasColumnName("IdTipoComp");
            this.Property(t => t.Moneda).HasColumnName("Moneda");
            this.Property(t => t.FechaFac).HasColumnName("FechaFac");
            this.Property(t => t.idTipoFact).HasColumnName("idTipoFact");
            this.Property(t => t.CalculoDetrac).HasColumnName("CalculoDetrac");
            this.Property(t => t.RegimenTrib).HasColumnName("RegimenTrib");
            this.Property(t => t.TipPago).HasColumnName("TipPago");
            this.Property(t => t.FecVencimiento).HasColumnName("FecVencimiento");
            this.Property(t => t.Estado).HasColumnName("Estado");
            this.Property(t => t.MedCanc).HasColumnName("MedCanc");
            this.Property(t => t.NumCheq).HasColumnName("NumCheq");

            // Relationships
            this.HasMany(t => t.Productoes)
                .WithMany(t => t.Facturas)
                .Map(m =>
                    {
                        m.ToTable("DetalleFactura");
                        m.MapLeftKey("IdFactura");
                        m.MapRightKey("idProd");
                    });

            this.HasOptional(t => t.Proveedor)
                .WithMany(t => t.Facturas)
                .HasForeignKey(d => d.IdProveedor);
            this.HasOptional(t => t.SubProyecto)
                .WithMany(t => t.Facturas)
                .HasForeignKey(d => d.IdProveedor);
            this.HasOptional(t => t.TipoComp)
                .WithMany(t => t.Facturas)
                .HasForeignKey(d => d.IdTipoComp);
            this.HasOptional(t => t.TipoFactura)
                .WithMany(t => t.Facturas)
                .HasForeignKey(d => d.idTipoFact);

        }
    }
}
