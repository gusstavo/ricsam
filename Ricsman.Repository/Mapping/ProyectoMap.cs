using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class ProyectoMap : EntityTypeConfiguration<Proyecto>
    {
        public ProyectoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdProyecto);

            // Properties
            // Table & Column Mappings
            this.ToTable("Proyecto");
            this.Property(t => t.IdProyecto).HasColumnName("IdProyecto");
            this.Property(t => t.NomProyecto).HasColumnName("NomProyecto");
        }
    }
}
