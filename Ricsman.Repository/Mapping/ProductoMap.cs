using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using Ricsman.Entities;

namespace Ricsman.Repository.Mapping
{
    public class ProductoMap : EntityTypeConfiguration<Producto>
    {
        public ProductoMap()
        {
            // Primary Key
            this.HasKey(t => t.IdProducto);

            // Properties
            // Table & Column Mappings
            this.ToTable("Producto");
            this.Property(t => t.IdProducto).HasColumnName("IdProducto");            
            this.Property(t => t.IcCategoria).HasColumnName("IcCategoria");

            // Relationships
            this.HasOptional(t => t.Categoria)
                .WithMany(t => t.Productoes)
                .HasForeignKey(d => d.IcCategoria);

        }
    }
}
