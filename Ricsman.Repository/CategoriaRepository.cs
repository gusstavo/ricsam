﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ricsman.Entities;

namespace Ricsman.Repository
{
    public interface ICategoriaRepository
    {
        List<Categoria> GetCategorias();
    }

    public class CategoriaRepository : MasterRepository, ICategoriaRepository
    {
        public List<Categoria> GetCategorias()
        {
            var query = from p in context.Categorias
                        select p;
            return query.ToList();

        }
    }
}
